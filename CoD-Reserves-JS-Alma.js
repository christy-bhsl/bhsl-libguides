<script>
function create_Html(doc) {
	crsid = doc['pnx']['search']['crsid']
	docid = doc['pnx']['search']['recordid']
	subj = doc['pnx']['search']['subject']
	isbn = doc['pnx']['search']['isbn']
	
	//try to get an image to display
	if (isbn) {
		imgUrl = "https://proxy-na.hosted.exlibrisgroup.com/exl_rewrite/syndetics.com/index.aspx?isbn=" + isbn[0] + "/SC.JPG&client=primo"
	} else {
		imgUrl = "https://bhslibrary-primo.hosted.exlibrisgroup.com/primo-explore/img/icon_book.png"
	}
	//some variable values have multiple indentical entries, so we need to clean those up
	crs =""
	for(var j in subj) {
		if (subj[j].startsWith("Course")) {
			crs += "</br>" + subj[j]
		}
	}

	document.getElementById("output-area").innerHTML += 
		"<tr><td> <img src='" + imgUrl + "' \"></td>" + 
		"<td><div style='margin-left:12px'><a href='https://bhslibrary-primo.hosted.exlibrisgroup.com/permalink/f/oquicj/" + docid[0] + "'>" +
		doc['pnx']['search']['title'] + "</a>" +
		crs + "</div></td></tr>";
}

function write_Json(docs) {
  for(var k in docs) {
        if(docs[k] instanceof Object) {
            create_Html(docs[k])
        } else {
            document.getElementById("output-area").innerHTML = data[k] + "<br>";
        };
    }
}

function get_Alma(val) {
	var request = new XMLHttpRequest()
	var url = "https://api-na.hosted.exlibrisgroup.com/primo/v1/search?vid=01TEXAM-HSC_V1&tab=blended&scope=Blended&q=" + val + "&lang=eng&offset=0&limit=100&sort=title&pcAvailability=true&getMore=0&conVoc=true&inst=01TEXAM-HSC&apikey=l8xxacc85048f9044927a335bca56c343bbd"
	
	//clear any previous search results
	document.getElementById("output-area").innerHTML = ""
		
	request.open('GET', url, true)
	request.onload = function() {
  		// Begin accessing JSON data here
  		var data = JSON.parse(this.response)
  		var docs = data["docs"]
		
  		if (request.status >= 200 && request.status < 400) {
   		 	write_Json(docs);
  		} else {
    		document.alert = "Gah, it's not working!";
  		}
	}
		
	//update the URL so that returning to the page will result in the same search 
	url = window.location.href
	
	if (location.hash){
		//clear out the hash before adding either the same or a new one
		idx = url.indexOf("#")
		cleanurl = url.slice(0,idx)
		newurl = cleanurl + "#" + val
		window.history.pushState("DB page", "Home - BHSL Databases - LibGuides at Baylor Health Sciences Library", newurl);
request.send()
	} else {
		newurl = url + "#" + val
		window.history.pushState("DB page", "Home - BHSL Databases - LibGuides at Baylor Health Sciences Library", newurl);
request.send()
	}
	
}

</script>