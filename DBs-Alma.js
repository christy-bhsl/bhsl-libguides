<script type="text/javascript">
function create_Html(doc) {
	descr = doc['pnx']['search']['description']
	docid = doc['pnx']['search']['recordid']
	title = doc['pnx']['search']['title']
	campus = doc['pnx']['addata']['notes']
	
	//determine is a description and/or a note are available for display
	if (descr && campus){
		disp = 1;
	} else if (descr && !campus){
		disp = 2;
	} else if (!descr && campus){
		disp = 3;
	} else if (!descr && !campus){
		disp = 4;
	}
	
	//just use the first recordid
	switch (disp){
		case 1:
			document.getElementById("output-area").innerHTML += 
			"<li><a href='" + 
			"https://bhslibrary-primo.hosted.exlibrisgroup.com/permalink/f/oquicj/" + docid[0] +
			"'>" + title + "</a></br>" +
			"<div style='margin-left:.5in'>" + descr +"</div></br>" +
		    "<div class='campus_note'>" + campus + "</div></li>";
			break;
		case 2:
			document.getElementById("output-area").innerHTML += 
			"<li><a href='" + 
			"https://bhslibrary-primo.hosted.exlibrisgroup.com/permalink/f/oquicj/" + docid[0] +
			"'>" + title + "</a></br>" +
			"<div style='margin-left:.5in'>" + descr +"</div></li>";
			break;			
		case 3:
			document.getElementById("output-area").innerHTML += 
			"<li><a href='" + 
			"https://bhslibrary-primo.hosted.exlibrisgroup.com/permalink/f/oquicj/" + docid[0] +
			"'>" + title + "</a></br>" +
		    "<div class='campus_note'>" + campus + "</div></li>";
			break;
		case 4:
			document.getElementById("output-area").innerHTML += 
			"<li><a href='" + 
			"https://bhslibrary-primo.hosted.exlibrisgroup.com/permalink/f/oquicj/" + docid[0] +
			"'>" + title + "</a></li>";
			break;
	}
}

function write_Json(docs, limit) {
	count = 0;
  for(var k in docs) {
		
        if(docs[k] instanceof Object) {
			//if a limit has been passed then we want only the entries that start with that letter
			if (limit != null) {
				title = docs[k]['pnx']['search']['title']
				
				if (title[0][0] == limit) {
					document.getElementById("output-area").innerHTML += "<ul>";
	            	create_Html(docs[k]);
					count++;
					document.getElementById("output-area").innerHTML += "</ul>";
				}
			} else {
				document.getElementById("output-area").innerHTML += "<ul>";
    	        create_Html(docs[k]);
				count++;
				document.getElementById("output-area").innerHTML += "</ul>";
			}
        } else {
            document.getElementById("output-area").innerHTML = data[k] + "<br>";
        };
    }
  document.getElementById("count-area").innerHTML = "<strong>" + count + "</strong> items"
}

function get_Alma(val, limit) {
	var request = new XMLHttpRequest()
	var url = "https://api-na.hosted.exlibrisgroup.com/primo/v1/search?vid=01TEXAM-HSC_V1&tab=databases&scope=Databases&q=" + val + "&lang=eng&offset=0&limit=200&sort=title&pcAvailability=true&getMore=0&conVoc=true&inst=01TEXAM-HSC&apikey=l8xxacc85048f9044927a335bca56c343bbd"
	
	//clear any previous search results
	document.getElementById("output-area").innerHTML = "Working..."
	document.getElementById("count-area").innerHTML = ""
		
	request.open('GET', url, true)
	request.onload = function() {
  		// Begin accessing JSON data here
  		var data = JSON.parse(this.response)
  		var docs = data["docs"]
		
  		if (request.status >= 200 && request.status < 400) {
			document.getElementById("output-area").innerHTML = ""
   		 	write_Json(docs, limit)
  		} else {
    		errorMessage.textContent = `Gah, it's not working!`
  		}
	}
	
	//set the top title to reflect the search
	if (val.includes("sub")){
		idx = val.lastIndexOf(",")
		subj = val.slice(idx+1)
		text = document.getElementById(subj).innerHTML
		document.getElementById("s-lg-guide-name").innerHTML = "DATABASE LIST BY SUBJECT: " + text;
	} else {
		if (limit == null) {
			document.getElementById("s-lg-guide-name").innerHTML = "DATABASE LIST BY TITLE: ALL";
		} else {
			document.getElementById("s-lg-guide-name").innerHTML = "DATABASE LIST BY TITLE: " + limit;
		}
	}
	
	//update the URL so that returning to the page will result in the same search 
	url = window.location.href
	
	if (location.hash){
		//clear out the hash before adding either the same or a new one
		idx = url.indexOf("#")
		cleanurl = url.slice(0,idx)
		newurl = cleanurl + "#" + val + "^" + limit
		window.history.pushState("DB page", "Home - BHSL Databases - LibGuides at Baylor Health Sciences Library", newurl);
request.send()
	} else {
		newurl = url + "#" + val + "^" + limit
		window.history.pushState("DB page", "Home - BHSL Databases - LibGuides at Baylor Health Sciences Library", newurl);
request.send()
	}
	
}

document.addEventListener("DOMContentLoaded", function() {
	if (location.hash){
		subst = location.hash.substr(1)
		len = location.hash.substr(1).indexOf('^')
		val = subst.slice(0,len)
		lim = subst.slice(len+1)
		if (lim == 'null') {limit = null} 
		   else {limit = lim}
		get_Alma(val, limit)
	}
}) 

</script>